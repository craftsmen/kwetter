package nl.craftsmen.kwetter

import kotlin.test.Test
import kotlin.test.assertTrue

class PlatformTest {
    @Test
    fun testGetPlatformName() {
        assertTrue(getPlatformName().isNotEmpty())
    }
}