package nl.craftsmen.kwetter.nl.craftsmen.ke

import nl.craftsmen.kwetter.getPlatformName
import kotlin.test.Test
import kotlin.test.assertEquals

class PlatformTest {
    @Test
    fun testGetPlatformName() {
        assertEquals("JVM on the desktop", getPlatformName())
    }
}