= Workshop Kotlin Multiplatform

== Agenda

- 13:00 Welkom
- 13:15 Uitleg MPP
- 13:30 Uitleg expect/actual oefening
- 13:40 Opdracht 1 (Eerste actual/expect fun)
- 14:00 Uitleg Compose GUI
- 14:10 Opdracht 2 (Eerste Compose GUI)
- 14:30 Pauze
- 14:50 Uitleg multiplatform dependencies
- 15:00 Opdracht 3 (Dependency toevoegen)
- 15:20 Uitleg Android target
- 15:30 Opdracht 4 (Android target toevoegen)
- 16:00 Uitleg van layouts in Compose
- 16:10 Opdracht 5 (GUI uitbouwen met lijst van berichten)
- 16:30 Uitleg van input in Compose
- 16:40 Opdracht 6 (GUI uitbouwen met invoerveld om berichten te sturen)
- 17:00 Einde

== Opdrachten

- Opdracht 1: Eerste actual/expect fun; platform name printen in console
- Opdracht 2: Eerste Compose GUI; window met daarin platform name
- Opdracht 3: Dependency toevoegen; kotlinx.datetime voor bepalen van huidige tijdstip
- Opdracht 4: Android target toevoegen (kost mogelijk extra tijd i.v.m. SDK installeren)
- Opdracht 5: GUI uitbouwen met lijst van berichten
- Opdracht 6: GUI uitbouwen met invoerveld om berichten te sturen (eigen username is hardcoded)
- Bonus opdracht (backend): Database voor berichten, bijv. ReThinkDB of firebase voor een live feed
- Bonus opdracht (frontend): Profiel pagina + navigatie tussen componenten

== Why

Support for multiplatform programming is one of Kotlin’s key benefits. It reduces time spent writing and maintaining the same code for different platforms while retaining the flexibility and benefits of native programming.

== Build tool

Kotlin Multiplatform project are built using Gradle. Apply the `kotlin-multiplatform` Gradle plugin in the file build.gradle.kts.
This plugin configures the project for creating an application or library to work on multiple platforms and prepares it for building on these platforms.

== Targets

A multiplatform project is aimed at multiple platforms that are represented by different targets. A target is part of the build that is responsible for building, testing, and packaging the application for a specific platform, such as macOS, iOS, or Android.

== Source sets

The project includes the directory `src` with Kotlin source sets, which are collections of Kotlin code files, along with their resources, dependencies, and language settings. A source set can be used in Kotlin compilations for one or more target platforms.

Source sets form a hierarchy, which is used for sharing the common code. In a source set shared among several targets, you can use the platform-specific language features and dependencies that are available for all these targets.

image::https://kotlinlang.org/docs/images/hierarchical-structure.png[source set hierarchy]

== Expect / Actual

If you’re developing a multiplatform application that needs to access platform-specific APIs that implement the required functionality, use the Kotlin mechanism of expected and actual declarations.

With this mechanism, a common source set defines an expected declaration, and platform source sets must provide the actual declaration that corresponds to the expected declaration. This works for most Kotlin declarations, such as functions, classes, interfaces, enumerations, properties, and annotations.

image::https://kotlinlang.org/docs/images/expect-actual.png[expect-actual]

A short example for logging a message. The expected declaration will look like this.

[source,kotlin]
----
// Common
enum class LogLevel {
    DEBUG, WARN, ERROR
}

internal expect fun writeLogMessage(message: String, logLevel: LogLevel)

fun logDebug(message: String) = writeLogMessage(message, LogLevel.DEBUG)
fun logWarn(message: String) = writeLogMessage(message, LogLevel.WARN)
fun logError(message: String) = writeLogMessage(message, LogLevel.ERROR)
----

For JVM, the actual declaration will look like this.

[source,kotlin]
----
// JVM
internal actual fun writeLogMessage(message: String, logLevel: LogLevel) {
    println("[$logLevel]: $message")
}
----

For JavaScript, a completely different set of APIs is available, and the actual declaration will look like this.

[source,kotlin]
----
// JS
internal actual fun writeLogMessage(message: String, logLevel: LogLevel) {
    when (logLevel) {
        LogLevel.DEBUG -> console.log(message)
        LogLevel.WARN -> console.warn(message)
        LogLevel.ERROR -> console.error(message)
    }
}
----

== Compilations

Each target can have one or more compilations, for example, for production and test purposes.

image::https://kotlinlang.org/docs/images/compilations.png[compilations overview]

== Resources

=== Kotlin MPP
- https://kotlinlang.org/docs/mpp-intro.html
- https://kotlinlang.org/docs/multiplatform-library.html
- https://play.kotlinlang.org/hands-on/Introduction%20to%20Kotlin%20Multiplatform
- https://play.kotlinlang.org/hands-on/Full%20Stack%20Web%20App%20with%20Kotlin%20Multiplatform
- https://www.bugsnag.com/blog/kotlin-multiplatform
- https://github.com/JetBrains/compose-jb/tree/master/templates/multiplatform-template

=== (JetBrains) Compose
- https://github.com/android/compose-samples/tree/master/Jetchat
- https://github.com/JetBrains/compose-jb/tree/master/tutorials
- https://github.com/JetBrains/compose-jb/tree/master/examples/todoapp

=== Decompose (Navigation)
- https://github.com/JetBrains/compose-jb/issues/85
- https://arkivanov.github.io/Decompose/samples/
- https://proandroiddev.com/a-comprehensive-hundred-line-navigation-for-jetpack-desktop-compose-5b723c4f256e
- https://proandroiddev.com/fully-cross-platform-kotlin-applications-almost-29c7054f8f28
- https://github.com/AAkira/Kotlin-Multiplatform-Libraries
- https://github.com/JetBrains/compose-for-web-demos
